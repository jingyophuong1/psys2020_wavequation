#include "pch.h"
#include "CppUnitTest.h"
#include"../Code/core.h"

using namespace Microsoft::VisualStudio::CppUnitTestFramework;

namespace testwavequation
{
	double sinfnck(double x)
	{
		return sin(x);
	}
	void isequal(double* a, double* b, int count)
	{
		for (size_t i = 0; i < count; i++)
		{
			double rounded1 = (int)(a[i] * 100000.0) / 100000.0;
			double rounded2 = (int)(b[i] * 100000.0) / 100000.0;
			Assert::AreEqual(rounded1, rounded2);
		}
	}
	
	TEST_CLASS(UnitTest1)
	{
	public:
		
		TEST_METHOD(TestMethod1_initial_amplitude_cal)
		{
			double cur_list[10];
			double x[10];
			double expected[10]{0, 0.841470985, 0.909297427, 0.141120008 ,-0.756802495,-0.958924275,-0.279415498,0.656986599,0.989358247,0.412118485};
			core::initial_amplitude_cal(&sinfnck, 10, 10, cur_list, x);
			isequal(expected, cur_list, 10);
		}
		TEST_METHOD(TestMethod2_amplitude_of_string_cal)
		{
		
			double finput[10]{ 0, 0.841470985, 0.909297427, 0.141120008 ,-0.756802495,-0.958924275,-0.279415498,0.656986599,0.989358247,0.412118485 };
			double* cur_list = (double*)malloc(10 * sizeof(double));
			double* latest_list = (double*)malloc(10 * sizeof(double));
			double* next_list = (double*)malloc(10 * sizeof(double));
			memcpy(cur_list, finput, 10 * sizeof(double));
			memcpy(latest_list, cur_list, 10 * sizeof(double));
			memcpy(next_list, cur_list, 10 * sizeof(double));
			int iteration = 5;
			while (iteration >0)
			{
				core::amplitude_of_string_with_openmp(cur_list, latest_list, next_list, 10, 0.1);
				auto buffer1 = cur_list;
				auto buffer2 = latest_list;
				auto buffer3 = next_list;
				next_list = buffer2;
				latest_list = buffer1;
				cur_list = buffer3;
				iteration--;
			}
			// iteration = 1
			//double expected[10]{0, 0.764106531, 0.825697041, 0.1281455,-0.687222423,-0.870761219,-0.253726166,0.596583554,0.898397106,0.412118485};
			// iteration = 5
			double expected[10]{ 0,-0.087821078,-0.094899862,-0.01472815,0.078984554,0.100083013,0.029476309,-0.059565338,0.000927538,0.412118485};
			for (size_t i = 0; i < 10; i++)
			{
				double rounded1 = (int)(expected[i] * 1000.0) / 1000.0;
				double rounded2 = (int)(cur_list[i] * 1000.0) / 1000.0;
				Assert::AreEqual(rounded1, rounded2);
			}
			free(next_list);
			free(latest_list);
			free(cur_list);
		}
	};
	
}



