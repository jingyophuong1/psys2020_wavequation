// Wavequation.cpp : This file contains the 'main' function. Program execution begins and ends there.
//

#include "core.h"
#include <iostream>
#include <math.h>
#include<stdlib.h>
double initial_function(double x);
double initial_function(double x)
{
	return sin(x);
}
int main(/*double argc, int argc1, char** argv*/)
{
	double length = 30; 
	double c =0.1;
	int pointNr = 100;

	double step = length/pointNr;
	double* initial_ampl_list = (double*)malloc(pointNr * sizeof(double));
	double* y = (double*)malloc(pointNr * sizeof(double));

	core::initial_amplitude_cal(*initial_function, pointNr, step, initial_ampl_list, y);
	
	
	double* lastest_ampl_list = (double*)malloc(pointNr * sizeof(double));
	for (int a = 0; a < pointNr; a++)
	{
		lastest_ampl_list[a] = 0;
	}
	double* next_ampl_list = (double*)malloc(pointNr * sizeof(double));
	int i = 0;
	
	while (i < 6)
	{
		core::amplitude_of_string(initial_ampl_list, lastest_ampl_list,next_ampl_list, pointNr, c);
		++i;
		
		for (int k = 1; k < pointNr - 1; k++)
		{
			printf("%lf\n", next_ampl_list[k]);
			initial_ampl_list[k] = next_ampl_list[k];
		}
		printf("---\n");
	
	}
	
	

	delete(initial_ampl_list);
	delete(lastest_ampl_list);
	delete(next_ampl_list);
	return  EXIT_SUCCESS;
	
	
}

// Run program: Ctrl + F5 or Debug > Start Without Debugging menu
// Debug program: F5 or Debug > Start Debugging menu

// Tips for Getting Started: 
//   1. Use the Solution Explorer window to add/manage files
//   2. Use the Team Explorer window to connect to source control
//   3. Use the Output window to see build output and other messages
//   4. Use the Error List window to view errors
//   5. Go to Project > Add New Item to create new code files, or Project > Add Existing Item to add existing code files to the project
//   6. In the future, to open this project again, go to File > Open > Project and select the .sln file
