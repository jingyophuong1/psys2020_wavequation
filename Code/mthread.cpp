#include "mthread.h"
#include<chrono>
mThread::mThread(QObject* parent):QThread(parent)
{
}
void mThread::run()
{
	int i = 1;
	auto start = std::chrono::steady_clock::now();
    while (!is_pause ) {
        is_cal_done = false;
		emit wave_cal();
        while (!is_cal_done) {
            msleep(1);
        }
		if (i == 1000)
		{
			auto dif = std::chrono::duration_cast<std::chrono::milliseconds>(std::chrono::steady_clock::now() - start).count();
			printf("%d\n", dif);
			i = 1;
			start = std::chrono::steady_clock::now();
		}
		i++;
    }
}
void mThread::cal_status(bool status)
{
    is_cal_done = status;
}
void mThread::pause(bool status)
{
    is_pause = status;
}
mThread::~mThread()
{
}
