#ifndef WAVEVISUALIZATION_H
#define WAVEVISUALIZATION_H

#include <QMainWindow>
#include"core.h"
#include"mthread.h"
namespace Ui {
class WaveVisualization;
}

class WaveVisualization : public QMainWindow
{
    Q_OBJECT

public:
	//! a QT winform to visualize the wave with the calculated amplitude in real time
    explicit WaveVisualization(QWidget *parent = 0);
    ~WaveVisualization();

private slots:
	//! the function is called whenn the thread started 
    void wave_cal();
	//! the function is called when user clicks on the "stop" - Button
    void threadStop();
	//! the function is called when user clicks on the "pause" - Button
    void threadPause();
	//! the function is called when user clicks on the "play" - Button 
    void threadRun();
	//! the function is called when user changes the option of the initial function
	void initfunc_changed(int index);
	//! by some options of the initial function such as p*sin(w*x + q) or p*cos(w*x + q), the user have to input the value for p , w and q. 
	//! the function is called when a such function is selected and p , w or q is changed. 
	void funcparams_changed();
	//! the function is called when user changes the values of the parameters such as length of string , c , steps 
	void initstand_changed();

	void clickedGraph(QMouseEvent* event);

	void mouseRelease(QMouseEvent* event);
	
signals:
    void cal_done(bool is_cal_done);
	
private:
    Ui::WaveVisualization *ui;
	double length = 0;
    double c = 0;
    int pointNr = 0;
    QVector<double>*x = nullptr;
	QVector<double>* cur_list = nullptr;
	QVector<double>* latest_list = nullptr;
	double (*initial_function)(double x) = nullptr;
	int hold_point = 0;
	//play: 0; pause: 1; stop= 2
	int status = 2;
#if OPENCL
	core* m_core = nullptr; 
#else
	QVector<double> *next_list = nullptr;
#endif
	mThread* mythread = nullptr;
};

#endif // WAVEVISUALIZATION_H
