__kernel void 
amplitude_of_point_cal(
	__global double* cur_amplitude_list, 
	__global double* latest_amplitude_list, 
	__global double* next_amplitude_list, 
	double c, 
	int pointNr)
{
	int i = get_global_id(0);
	double value = 0;
	if (i == 0 || i == pointNr - 1)
	{
		value = cur_amplitude_list[i];
	}
	else
	{
		double A_i_t = cur_amplitude_list[i];
		double A_i_t1 = latest_amplitude_list[i];
		double A_i1_t = i > 0 ? cur_amplitude_list[i - 1] : 0;
		double A_ip1_t = i < pointNr - 1 ? cur_amplitude_list[i + 1] : 0;
		value = 2.0 * A_i_t - A_i_t1 + c * (A_i1_t - 2.0 * A_i_t + A_ip1_t);
	}
	next_amplitude_list[i] = value;
}