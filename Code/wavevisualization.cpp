#include "wavevisualization.h"
#include "ui_wavevisualization.h"

double p = 1; double w = 1; double q = 0;

double sinfnc(double x)
{
	return p * sin(x*w + q);
}
double cosfnc(double x)
{
	return p * cos(x*w + q);
}

WaveVisualization::WaveVisualization(QWidget *parent):
    QMainWindow(parent),
    ui(new Ui::WaveVisualization)
{
    ui->setupUi(this);
   // WaveVisualization::makePlot();
	// give the axes some labels:
	connect(ui->customPlot, SIGNAL(mousePress(QMouseEvent*)), this, SLOT(clickedGraph(QMouseEvent*)));
	connect(ui->customPlot, SIGNAL(mouseRelease(QMouseEvent*)), this, SLOT(mouseRelease(QMouseEvent*)));
	ui->customPlot->xAxis->setLabel("x");
	ui->customPlot->yAxis->setLabel("y");
	ui->customPlot->yAxis->setRange(-2, 2);
	if (initial_function == nullptr) initial_function = &sinfnc;
	initstand_changed();
}

WaveVisualization::~WaveVisualization()
{
 if(status != 2) threadStop();
	delete ui;
}
void WaveVisualization::wave_cal()
{
	if (cur_list != nullptr && latest_list != nullptr)
	{
		ui->customPlot->clearGraphs();
		ui->customPlot->addGraph();

		ui->customPlot->graph(0)->setData(*x, *cur_list);

#if OPENCL
		m_core->amplitude_of_string_with_opencl(cur_list->data());
#else
		core::amplitude_of_string_with_openmp(cur_list->data(), latest_list->data(), next_list->data(), pointNr, hold_point, c);
		cur_list->swap(*latest_list);
		next_list->swap(*cur_list);
#endif
		ui->customPlot->replot();
		ui->customPlot->update();
		emit cal_done(true);
	}
}
void WaveVisualization::threadStop()
{
	ui->gbparams->setDisabled(false);
	ui->customPlot->clearGraphs();
	ui->customPlot->replot();
	ui->customPlot->update();
	if (mythread != nullptr)
	{
		if (mythread->isRunning()) mythread->pause(true);
		mythread->terminate();
		delete mythread;
		mythread = nullptr;
	}
#if OPENCL
	if (m_core != nullptr)
	{
		delete m_core;
	}
#else 
	delete next_list;
	next_list = nullptr;
#endif // OPENMP

    delete x;
	x = nullptr;
	delete cur_list;
	cur_list = nullptr;
	delete latest_list;
	latest_list = nullptr;

	initstand_changed();
	status = 2;
}
void WaveVisualization::threadPause()
{
    mythread->pause(true);
	status = 1;
	
}
void WaveVisualization::initstand_changed()
{
	length = ui->sblength->value();
	pointNr = ui->sbsteps->value();
	if (length <= 0 || pointNr <= length)
	{
		auto mes = QMessageBox::information(this, "Invalid parameters", "The lenght of string muss be positiv and less than the steps", QMessageBox::Ok);
	}
	else
	{
		if (x != nullptr) delete x;
		x = new QVector<double>(pointNr);
		if (cur_list != nullptr) delete cur_list;
		cur_list = new QVector<double>(pointNr);
		
		core::initial_amplitude_cal(initial_function, pointNr, length, cur_list->data(), x->data());
		// set axes ranges, so we see all data:
		ui->customPlot->xAxis->setRange(0, length);
		ui->customPlot->clearGraphs();
		ui->customPlot->addGraph();
		ui->customPlot->graph(0)->setData(*x, *cur_list);
		ui->customPlot->replot();
		ui->customPlot->update();
	}
		
}
void WaveVisualization::threadRun()
{
    if(mythread == nullptr)
    {

		ui->gbparams->setDisabled(true);
		c = ui->sbc->value();
		latest_list = new QVector<double>(pointNr);
		*latest_list = *cur_list;
		mythread = new mThread(this);

#if OPENCL
		m_core = new core();
		m_core->opencl_initial(cur_list->data(), latest_list->data(), pointNr, c);
#else 
		next_list = new QVector<double>(pointNr);
		*next_list = *cur_list;
#endif
		connect(mythread, SIGNAL(wave_cal()), this, SLOT(wave_cal()));
		connect(this, SIGNAL(cal_done(bool)), mythread, SLOT(cal_status(bool)));
		mythread->pause(false);
		mythread->start();
    }
    else if(!mythread->isRunning())
    {
        mythread->pause(false);
        mythread->start();
    }
	status = 0;
}
void WaveVisualization::initfunc_changed(int index)
{
	switch (index)
	{
	case 0: 
		p = 1; w = 1; q = 0;
		initial_function= &sinfnc;
		break;
	case 1:
		p = 1; w = 1; q = 0;
		initial_function = &cosfnc;
		break;
	case 2:
		p = ui->sbp->value(); w = ui->sbw->value(); q = ui->sbq->value();
		initial_function = &sinfnc;
		break;
	case 3:
		p = ui->sbp->value(); w = ui->sbw->value(); q = ui->sbq->value();
		initial_function = &cosfnc;
		break;
	default:
		break;
	}
	core::initial_amplitude_cal(initial_function, pointNr, length, cur_list->data(), x->data());
	ui->customPlot->xAxis->setRange(0, length);
	ui->customPlot->clearGraphs();
	ui->customPlot->addGraph();
	ui->customPlot->graph(0)->setData(*x, *cur_list);
	ui->customPlot->replot();
	ui->customPlot->update();
}
void WaveVisualization::funcparams_changed()
{
	int func_index = ui->comboBox->currentIndex();
	if (func_index >= 2)
	{
		initfunc_changed(func_index);
	}
}
void WaveVisualization::clickedGraph(QMouseEvent* event)
{
	QPoint point = event->pos();
	auto step = length / pointNr;
	hold_point = (int)(ui->customPlot->xAxis->pixelToCoord(point.x()) / step);
	if (abs(cur_list->at(hold_point) - ui->customPlot->yAxis->pixelToCoord(point.y())) > 0.1)
	{
		hold_point = 0;
	}
}
void WaveVisualization::mouseRelease(QMouseEvent* event)
{
	hold_point = 0;
}