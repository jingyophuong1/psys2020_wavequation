/*
	define OPENCL and VISUAL in core.h
*/
#include "core.h"
#if VISUAL
#include "wavevisualization.h"
#include <QtWidgets/QApplication>
int main(int argc, char *argv[])
{
	QApplication a(argc, argv);
	WaveVisualization w;
	w.show();
	return a.exec();

}
#else //Without VISUAL
#include <stdio.h>
#include <chrono>
#include"math.h"
#include"stdlib.h"
#include<string.h>
double sinfnck(double x)
{
	return sin(x);
}
int main(int argc, char **argv)
{

	double length; 
	double c;
	int steps;

	if (argc != 4)
	{
		fprintf(stderr, "Not enough arguments given!\n");
		return EXIT_FAILURE;
	}
	length = atof(argv[1]);
	c = atof(argv[2]);
	steps = atoi(argv[3]);
	
	double* cur = (double*)malloc(steps * sizeof(double));

	double* last = (double*)malloc(steps * sizeof(double));

	core::initial_amplitude_cal(&sinfnck, steps, length, cur, last);

#if OPENCL
	int i = 1000;
	core m_core;
	m_core.opencl_initial(cur, last, steps, c);

	auto start = std::chrono::steady_clock::now();
	while (i > 0)
	{
		m_core.amplitude_of_string_with_opencl(cur);
		i--;
	}
	auto dif = std::chrono::duration_cast<std::chrono::milliseconds>(std::chrono::steady_clock::now() - start).count();
	printf("%d\n", dif);
#else // OPENMP
	int i = 1000;
	double* next = (double*)malloc(steps * sizeof(double));

	auto start = std::chrono::steady_clock::now();

	while (i > 0)
	{
		/* FIXME: These two memcpy()s increase the sequential part of the progam
		 *  unnecessarily. Just toggle the role of the pointers to reach the same goal.
		 *  Also modifiy the sequential version (if there is any) and the OpenCL part if
		 *  the same pattern is there.
		 */
	//	memcpy(last, cur, steps * sizeof(double*));
	//	memcpy(cur, next, steps * sizeof(double*));
		auto buffer1 = last;
		auto buffer2 = cur;
		auto buffer3 = next;
		next = buffer1;
		last = buffer2;
		cur = buffer3;
		core::amplitude_of_string_with_openmp(cur, last, next, steps, c);
		i--;
	}
	auto dif = std::chrono::duration_cast<std::chrono::milliseconds>(std::chrono::steady_clock::now() - start).count();
	printf("%ld\n", dif);
	free(next);
#endif // OPENCL
	free(cur);
	free(last);
	return 0;
}
#endif // VISUAL
