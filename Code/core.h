﻿#ifndef CORE_H
#define CORE_H
#define OPENCL false
#define VISUAL false
#include<math.h>
#include<stdio.h>
#include<stdlib.h>
#include<iterator>
#if OPENCL
#include <CL/cl.h>
#else 
#include<omp.h>
#endif
using namespace std;
class core
{
public:
    core();
	~core();
	//! calculate the first amplitude of all points of the string 
	/*!
	\param f(x) Initialfunction of the wave
	\param pointNr Number of points whose amplitude should be calculated.
	\param length Length of the string
	\param output_y Pointer to all values of the calculated amplitude
	\param output_x Pointer to values of the position of all point in the string*/
    static void initial_amplitude_cal(double(*f) (double x), int pointNr, double length, double* output_y, double* output_x);
	


#if OPENCL
public: 

	int opencl_initial(double* cur_amplitude_list, double* latest_amplitude_list, int pointNr, double c);
	void amplitude_of_string_with_opencl(double* output);
private:


	//! read OpenCL kernel and load it in a given buffer
	/*!
	\param path path to the OpenCL file
	\param buf pointer to the buffer
	*/
	static long LoadOpenCLKernel(char const* path, char **buf);

	int err;								// error code returned from api calls

	cl_device_id device_id;					// compute device id 
	cl_context context;						// compute context
	cl_command_queue commands;				// compute command queue
	cl_program program;						// compute program
	cl_kernel kernel;						// compute kernel

	// OpenCL device memory for all lists
	cl_mem d_C;								// current
	cl_mem d_L;								// latest
	cl_mem d_N;								// next
	size_t globalWorkSize;
	unsigned int mem_size;
#else OPENMP
	//! calculate the amplitude at the next time (t+1) of all points of the string
	/*!
	\param cur_amplitude_list pointer to the amplitude at the current time (t) of all points
	\param latest_amplitude_list pointer to the calculated amplitude at the last time (t-1) of all points
	\param next_amplitude_list pointer to the returned list
	\param pointNr Number of points whose amplitude should be calculated.
	\param c a constant, which is defined from user and takes part in the calculation*/
	static void amplitude_of_string_with_openmp(double* cur_amplitude_list, double* latest_amplitude_list, double* next_amplitude_list, int pointNr, double c);
	//! calculate the amplitude at the next time (t+1) of all points of the string
	/*!
	\param cur_amplitude_list pointer to the amplitude at the current time (t) of all points
	\param latest_amplitude_list pointer to the calculated amplitude at the last time (t-1) of all points
	\param next_amplitude_list pointer to the returned list
	\param hold_point the pointer, which is held on the string except for the two ends
	\param pointNr Number of points whose amplitude should be calculated.
	\param c a constant, which is defined from user and takes part in the calculation*/
	static void amplitude_of_string_with_openmp(double* cur_amplitude_list, double* latest_amplitude_list, double* next_amplitude_list, int pointNr, int hold_point, double c);
private:
	static void next_amplitude_of_point_cal(double* cur_amplitude_list, double* latest_amplitude_list, double* next_amplitude_list, int point_index, double c, int pointNr);

#endif // OPENCL


};

#endif // CORE_H
