﻿#include "core.h"


using namespace std;
core::core()
{
	
}
void core::initial_amplitude_cal(double(*f) (double x), int pointNr,
	double length, double*initial_ampl, double* x)
{
	double step = length / pointNr;
    for (int  i = 0; i < pointNr ; i++)
    {
        initial_ampl[i] = f(i*step);
        x[i] = i*step;
    }
}

// -------------------------------------------------------------------------------- //
#if OPENCL

long core::LoadOpenCLKernel(char const* path, char **buf)
{
	FILE  *fp;
	size_t fsz;
	long   off_end;
	int    rc;

	/* Open the file */
	fp = fopen(path, "rb");
	if (NULL == fp) {
		return -1L;
	}

	/* Seek to the end of the file */
	rc = fseek(fp, 0L, SEEK_END);
	if (0 != rc) {
		return -1L;
	}

	/* Byte offset to the end of the file (size) */
	if (0 > (off_end = ftell(fp))) {
		return -1L;
	}
	fsz = (size_t)off_end;

	/* Allocate a buffer to hold the whole file */
	*buf = (char *)malloc(fsz + 1);
	if (NULL == *buf) {
		return -1L;
	}

	/* Rewind file pointer to start of file */
	rewind(fp);

	/* Slurp file into buffer */
	if (fsz != fread(*buf, 1, fsz, fp)) {
		free(*buf);
		return -1L;
	}

	/* Close the file */
	if (EOF == fclose(fp)) {
		free(*buf);
		return -1L;
	}


	/* Make sure the buffer is NUL-terminated, just in case */
	(*buf)[fsz] = '\0';

	/* Return the file size */
	return (long)fsz;
}

int core::opencl_initial(double* cur_amplitude_list, double* latest_amplitude_list,
	int pointNr, double c)
{
	// Allocate host memory for all lists
	mem_size = sizeof(cur_amplitude_list)*pointNr;

	// Get platform ids
	cl_uint dev_cnt = 0;
	clGetPlatformIDs(0, 0, &dev_cnt);

	cl_platform_id platform_ids[100];
	clGetPlatformIDs(dev_cnt, platform_ids, NULL);


	// Connect to a compute device
	int gpu = 1;
	err = clGetDeviceIDs(platform_ids[0], gpu ? CL_DEVICE_TYPE_GPU : CL_DEVICE_TYPE_CPU, 1, &device_id, NULL);
	if (err != CL_SUCCESS)
	{
		printf("Error: Failed to create a device group!\n");
		return err;
	}

	// Create a compute context 
	context = clCreateContext(0, 1, &device_id, NULL, NULL, &err);
	if (!context)
	{
		printf("Error: Failed to create a compute context!\n");
		return err;
	}

	// Create a command commands
	commands = clCreateCommandQueue(context, device_id, 0, &err);
	if (!commands)
	{
		printf("Error: Failed to create a command commands!\n");
		return err;
	}

	// Create the compute program from the source file
	char *KernelSource;
	long lFileSize;

	lFileSize = LoadOpenCLKernel("Wavequation.cl", &KernelSource);
	if (lFileSize < 0L) {
		perror("File read failed");
		return err;
	}

	program = clCreateProgramWithSource(context, 1, (const char **)&KernelSource, NULL, &err);
	if (!program)
	{
		printf("Error: Failed to create compute program!\n");
		return err;
	}

	// Build the program executable
	err = clBuildProgram(program, 0, NULL, NULL, NULL, NULL);
	if (err != err)
	{
		size_t len;
		char buffer[2048];
		printf("Error: Failed to build program executable!\n");
		clGetProgramBuildInfo(program, device_id, CL_PROGRAM_BUILD_LOG, sizeof(buffer), buffer, &len);
		printf("%s\n", buffer);
	}

	// Create the compute kernel in the program we wish to run
	kernel = clCreateKernel(program, "amplitude_of_point_cal", &err);
	if (!kernel || err != CL_SUCCESS)
	{
		printf("Error: Failed to create compute kernel!\n");
	}
	// Create the input and output arrays in device memory for our calculation
	d_C = clCreateBuffer(context, CL_MEM_READ_WRITE | CL_MEM_COPY_HOST_PTR, mem_size, cur_amplitude_list, &err);
	d_L = clCreateBuffer(context, CL_MEM_READ_WRITE | CL_MEM_COPY_HOST_PTR, mem_size, latest_amplitude_list, &err);
	d_N = clCreateBuffer(context, CL_MEM_WRITE_ONLY, mem_size, NULL, &err);

	if (!d_C || !d_L || !d_N)
	{
		printf("Error: Failed to allocate device memory!\n");
	}
	// Set kernel arguments
	err = clSetKernelArg(kernel, 0, sizeof(cl_mem), (void *)&d_C);
	err |= clSetKernelArg(kernel, 1, sizeof(cl_mem), (void *)&d_L);
	err |= clSetKernelArg(kernel, 2, sizeof(cl_mem), (void *)&d_N);
	err |= clSetKernelArg(kernel, 3, sizeof(double), (void *)&c);
	err |= clSetKernelArg(kernel, 4, sizeof(unsigned int), (void *)&pointNr);


	if (err != CL_SUCCESS)
	{
		printf("Error: Failed to set kernel arguments! %d\n", err);
	}
	globalWorkSize = pointNr;
	err = 0;
	return err;
}
void core::amplitude_of_string_with_opencl(double* output)
{
	err = clEnqueueNDRangeKernel(commands, kernel, 1, NULL, &globalWorkSize, NULL, 0, NULL, NULL);

	//if (err != CL_SUCCESS)
	//{
	//	printf("Error: Failed to execute kernel! %d\n", err);
	//	exit(1);
	//}

	//Retrieve result from device
	err = clEnqueueReadBuffer(commands, d_N, CL_FALSE, 0, mem_size, output, 0, NULL, NULL);
	/*if (err != CL_SUCCESS)
	{
		printf("Error: Failed to read output array! %d\n", err);
		exit(1);
	}*/
	err = clEnqueueCopyBuffer(commands, d_C, d_L, 0, 0, mem_size, 0, NULL, NULL);
	/*if (err != CL_SUCCESS)
	{
		printf("Error: Failed to copy buffer! %d\n", err);
		exit(1);
	}*/
	err = clEnqueueCopyBuffer(commands, d_N, d_C, 0, 0, mem_size, 0, NULL, NULL);
	//if (err != CL_SUCCESS)
	//{
	//	printf("Error: Failed to copy buffer! %d\n", err);
	//	exit(1);
	//}

}

#else

void core::amplitude_of_string_with_openmp(double* cur_amplitude_list, double* latest_amplitude_list, double* next_amplitude_list, int pointNr, double c)
{
#pragma omp parallel for num_threads(omp_get_max_threads()/2)
	for (int i = 1; i < pointNr - 1; ++i)
	{
		next_amplitude_of_point_cal(cur_amplitude_list, latest_amplitude_list, next_amplitude_list, i, c, pointNr);
	}
}

void  core::next_amplitude_of_point_cal(double* cur_amplitude_list, double*latest_amplitude_list, double* next_amplitude_list, 
	int point_index, double c, int pointNr)
{
	double A_i_t = cur_amplitude_list[point_index];
	double A_i_t1 = latest_amplitude_list[point_index];
	double A_i1_t = point_index > 0 ? cur_amplitude_list[point_index - 1] : 0;
	double A_ip1_t = point_index < pointNr - 1 ? cur_amplitude_list[point_index + 1] : 0;
	next_amplitude_list[point_index] = 2.0 * A_i_t - A_i_t1 + c * (A_i1_t - 2.0 * A_i_t + A_ip1_t);
}

void core::amplitude_of_string_with_openmp(double* cur_amplitude_list, double* latest_amplitude_list, double* next_amplitude_list,
	int pointNr, int hold_point, double c)
{
	auto ntn = omp_get_max_threads()/2;
#pragma omp parallel for num_threads(ntn)
	for (int i = 1; i < hold_point - 1; ++i)
	{
		next_amplitude_of_point_cal(cur_amplitude_list, latest_amplitude_list, next_amplitude_list, i, c, pointNr);
	}
#pragma omp parallel for num_threads(ntn)
	for (int i = hold_point + 1; i < pointNr - 1; ++i)
	{
		next_amplitude_of_point_cal(cur_amplitude_list, latest_amplitude_list, next_amplitude_list, i, c, pointNr);
	}

}
#endif // OPENCL


core::~core()
{
#if OPENCL
	if (err == 0)
	{
		clReleaseMemObject(d_C);
		clReleaseMemObject(d_L);
		clReleaseMemObject(d_N);

		clReleaseProgram(program);
		clReleaseKernel(kernel);
		clReleaseCommandQueue(commands);
		clReleaseContext(context);
		err = -1;
	}
#endif // OPENCL

	
}