#ifndef THREAD_H
#define THREAD_H
#include "qthread.h"
class mThread : public QThread
{
    Q_OBJECT
public:
    explicit mThread(QObject* parent);
	//! the run function is called when the thread started
    void run();
	//! changes the status of the thread
	/*!
	\param status if it is true, the thread will be paused*/
    void pause(bool status);
     ~mThread();
 signals:
	//! wenn the thread started, this function should be called as signal to the slots in GUI form. 
    void wave_cal();
public slots:
	//! wenn the calculation of the amplitude of all point is done, a signal from the GUI should be emitted and connected to this slot
	//! the next calculation can by be started for the next point of time 
    void cal_status(bool status);

private:
    bool is_pause = false;
    bool is_cal_done = false;
};

#endif // THREAD_H
